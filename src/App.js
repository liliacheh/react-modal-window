import Button from './components/button';
import '../src/styles/style.scss';
import Modal from './components/modal';
import { modalProps } from './components/modal/modalProps';
import { Component } from 'react';

class App extends Component {
  constructor(props){
    super(props);

    this.state = { isOpen : null }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
}

openModal (e) {
  const id = e.target.dataset.modalId
  this.setState({isOpen: id})
};

closeModal(){
  this.setState({isOpen: null})
}

  render (){
    return(
   <>
   {this.state.isOpen 
   && <Modal 
   data={modalProps.find(modal => modal.id === this.state.isOpen)}
   closeModal= {this.closeModal}
   closeButton={modalProps.find(modal => modal.id === this.state.isOpen).closeButton}/> }
   <Button text='Open first modal' modalId='m1' backgroundColor='#76dbf5' openModal={this.openModal} />
   <Button text='Open second modal' modalId='m2'backgroundColor='#a790f7' openModal={this.openModal}/>
   </>
    )
};
}
export default App;
