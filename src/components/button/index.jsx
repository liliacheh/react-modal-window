import { Component } from "react";

export default class Button extends Component {
  
    render(){
        const { backgroundColor, text , openModal, modalId} = this.props;
        return(
        <button className="btn"
        data-modal-id={modalId} 
        style={{backgroundColor: backgroundColor}}
        onClick={openModal}
        >{text}</button>
        )
    }
}
