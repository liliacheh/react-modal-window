import { Component } from "react";

export default class Modal extends Component {
 
    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    handleClickOutside = (e) => {
        const modal = document.querySelector(".modal__body");
        if (modal && !modal.contains(e.target)) {
            this.props.closeModal();
        }
    };
    render(){
    const modalProps = this.props.data
    const props = this.props

        return(
            <div className="modal">
                <div className="modal__body">
                    <div className="modal__header">
                    <h3 className="modal__title">{modalProps.header}</h3>
                    {props.closeButton && modalProps.closeBtn("modal__close", props.closeModal)}
                    </div>
                    {modalProps.text}
                    {modalProps.actions && modalProps.actions("modal__btn",props.closeModal, props.closeModal)}
                </div>
            </div>
        )
    }
}