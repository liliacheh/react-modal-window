export const modalProps = [{
    id: 'm1',
    header: "Do you whant to delete this file?",
    closeButton: true,
    text:<><div className="modal__text"><p>Once you delete this file? it won't be possible to undo this action.</p>
    <p>Are you sure you want to delete it?</p></div></> ,

    closeBtn(className, closeHandler ){
        return(
            <button className={className} onClick={closeHandler}></button>
        )
    },
    actions(className, closeHandler, submitHandler){
        return(
            <>
     <div className="modal__btns">
     <button className={className} onClick={submitHandler}>Ok</button>
     <button className={className} onClick={closeHandler}>Cancel</button>
     </div>
            </>
        )
    }
}
,
{
    id: 'm2',
    header: "The product has been added to your cart",
    closeButton: false,
    text: <><div className="modal__text"><p>Do you want to continue shopping or go to your shopping cart?</p></div></>,
    
    closeBtn(className, closeHandler ){
        return(
        <button className={className} onClick={closeHandler}></button>
    )
},
    actions(className, shopHandler, cartHandler){
        return(
        <>
 <div className="modal__btns">
 <button className={className} onClick={shopHandler}>Сontinue shopping</button>
 <button className={className} onClick={cartHandler}>Go to cart</button>
</div>
        </>
    )
}
}
]